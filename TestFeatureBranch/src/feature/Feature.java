package feature;

public class Feature {
    private String name;
    private String address;

    public Feature(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "Feature(" + name + ", " + address + ")";
    }

}
